package com.example.usblistendemo;

import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class USBBroadcastReceiver extends BroadcastReceiver {
	private Timer mTimer = new Timer();

	@Override
	public void onReceive(final Context context, Intent intent) {
		if (intent.getAction().equals("android.hardware.usb.action.USB_STATE")) {
			if (intent.getExtras().getBoolean("connected")) {
				final Intent recordIntent = new Intent();
				recordIntent.setClass(context, BackgroundVideoRecorder.class);
				context.startService(recordIntent);
				mTimer.schedule(new TimerTask() {

					@Override
					public void run() {
						context.stopService(recordIntent);
					}
				}, 10000);
			} else {
				try {
					Intent recordIntent = new Intent();
					recordIntent.setClass(context,
							BackgroundVideoRecorder.class);
					context.stopService(recordIntent);
				} catch (Exception e) {
				}
			}
		}
	}

}
