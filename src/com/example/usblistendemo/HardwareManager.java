package com.example.usblistendemo;

import java.lang.reflect.Method;

import android.os.IHardwareService;

import android.os.IBinder;

public class HardwareManager {

	public static void setFlashlightEnable(boolean enable) {
		try {
			Method method = Class.forName("android.os.ServiceManager")
					.getMethod("getService", String.class);
			IBinder binder = (IBinder) method.invoke(null,
					new Object[] { "hardware" });
			IHardwareService localHardwareService = IHardwareService.Stub
					.asInterface(binder);
			localHardwareService.setFlashlightEnabled(enable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
