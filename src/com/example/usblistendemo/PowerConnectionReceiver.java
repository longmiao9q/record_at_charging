package com.example.usblistendemo;

import java.util.Iterator;
import java.util.List;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;

public class PowerConnectionReceiver extends WakefulBroadcastReceiver {
	private Context mContext;
	private int record_strategy = 1;

	@Override
	public void onReceive(final Context context, Intent intent) {
		SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(context);
		
		boolean isOpenRecordOnCharging = preference.getBoolean("record_open", true);
		if (!isOpenRecordOnCharging) {
			return;
		}
		mContext = context;
		String action = intent.getAction();
		final Intent recordIntent = new Intent();
		if ("android.intent.action.ACTION_POWER_CONNECTED".equals(action)) {
			HardDiskUtils.deleteRecordFilesToSupportEnoughSpace(context);
			recordIntent.setClass(context, BackgroundVideoRecorder.class);
			startWakefulService(context, recordIntent);
			SharedPreferences pre = PreferenceManager.getDefaultSharedPreferences(mContext);
			record_strategy = Integer.valueOf(pre.getString("record_strategy_value", "1"));
			if (1 == record_strategy) {
				startCheckToStopRecord(context);
			}
		} else if ("android.intent.action.ACTION_POWER_DISCONNECTED"
				.equals(action)) {
			SharedPreferences pre = PreferenceManager.getDefaultSharedPreferences(mContext);
			record_strategy = Integer.valueOf(pre.getString("record_strategy_value", "1"));
			if (record_strategy == 2
					&& searchService(context,
							"com.example.usblistendemo.BackgroundVideoRecorder")) {
				try {
					Intent closeInent = new Intent();
					closeInent.setClass(context, BackgroundVideoRecorder.class);
					context.stopService(closeInent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if ("com.example.usblistendemo.check_ischaring".equals(action)) {
			Toast.makeText(context, "check_ischaring", Toast.LENGTH_SHORT)
					.show();
			if (isCharging(context)) {
				startCheckToStopRecord(context);
			} else {
				try {
					Intent closeInent = new Intent();
					closeInent.setClass(context, BackgroundVideoRecorder.class);
					context.stopService(closeInent);
				} catch (Exception e) {
					Toast.makeText(context, e.toString(), Toast.LENGTH_LONG)
							.show();
				}
			}
		}
	}

	private void startCheckToStopRecord(Context context) {
		Intent intent = new Intent();
		intent.setAction("com.example.usblistendemo.check_ischaring");
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager am = (AlarmManager) context
				.getSystemService(context.ALARM_SERVICE);
		if (null != am)
			am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 10000,
					sender);
	}

	private boolean isCharging(Context context) {
		IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batteryStatus = context.registerReceiver(null, ifilter);
		int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
		boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING
				|| status == BatteryManager.BATTERY_STATUS_FULL;
		return isCharging;
	}

	public static boolean searchService(Context context, String serviceName) {
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> l = am
				.getRunningServices(Integer.MAX_VALUE);
		Iterator<ActivityManager.RunningServiceInfo> i = l.iterator();
		while (i.hasNext()) {
			ActivityManager.RunningServiceInfo runningServiceInfo = i.next();
			if (runningServiceInfo.service.getClassName().equals(serviceName)) {
				// packageName = runningServiceInfo.service.getPackageName();
				return true;
			}
		}
		return false;
	}
}
