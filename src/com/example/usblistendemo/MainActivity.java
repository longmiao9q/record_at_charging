package com.example.usblistendemo;

import java.io.File;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapLabel;

import android.support.v7.app.ActionBarActivity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends ActionBarActivity {
	private SensorManager sensorManager;
	private Sensor sensor;
	private BootstrapLabel mTv;
	private BootstrapLabel mHdTv;
	private BootstrapButton mOpenRecordDir;
	private BootstrapButton mOpenFlashlight;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTv = (BootstrapLabel) findViewById(R.id.prompt_tv);
		mHdTv = (BootstrapLabel) findViewById(R.id.hd_tv);
		mOpenFlashlight = (BootstrapButton) findViewById(R.id.open_flashlight);
		mOpenFlashlight.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				HardwareManager.setFlashlightEnable(true);
			}
		});
		mOpenRecordDir = (BootstrapButton) findViewById(R.id.open_record_dir);
		mHdTv.setText("可用磁盘容量：" + getHD());
		mOpenRecordDir.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String dirPath = Environment.getExternalStorageDirectory()
						+ "/" + "recordWhenCharging/";
				File file = new File(dirPath);
				if (null == file || !file.exists()) {
					return;
				}
				Intent intent = new Intent(Intent.ACTION_PICK);
				intent.addCategory(Intent.CATEGORY_DEFAULT);
				Uri uri = Uri.fromFile(file);
				intent.setDataAndType(uri, "video/mp4");
				try {
					startActivityForResult(intent, 0x01);
				} catch (ActivityNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

		sensorManager.registerListener(new SensorEventListener() {

			@Override
			public void onSensorChanged(SensorEvent event) {
				float lux = event.values[0];
				mTv.setText("lux：" + lux);
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {

			}
		}, sensor, SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent data) {
		super.onActivityResult(arg0, arg1, data);
		if (arg0 == 0x01 && data != null) {

			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.addCategory(Intent.CATEGORY_DEFAULT);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setData(data.getData());
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent settingIntent = new Intent(this, SettingActivity.class);
			startActivity(settingIntent);
		}
		return super.onOptionsItemSelected(item);
	}

	private String getHD() {
		File root = Environment.getExternalStorageDirectory();
		StatFs sf = new StatFs(root.getPath());
		long blockSize = sf.getBlockSize();
		long availCount = sf.getAvailableBlocks();
		return (availCount * blockSize) / 1024 / 1024 + "MB";
	}
}
