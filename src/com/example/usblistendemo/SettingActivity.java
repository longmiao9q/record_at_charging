package com.example.usblistendemo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class SettingActivity extends ActionBarActivity {

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, new SettingFragment()).commit();
	}
}
